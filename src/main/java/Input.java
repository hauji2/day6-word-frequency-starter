public class Input {
    private String value;
    private int count;

    public Input(String w, int i) {
        this.value = w;
        this.count = i;
    }


    public int getWordCount() {
        return this.count;
    }

    public String toWordFrequencySentence() {
        return this.value + " " + this.count;
    }


}
