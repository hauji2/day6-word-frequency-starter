public class WordFrequencyConstant {
    public static final String REGEX_SPLITTER = "\\s+";
    public static final String DELIMITER = "\n";
    public static final String CALCULATE_ERROR = "Calculate Error";
    public static final String ONE_WITH_PREFIX_SPACE = " 1";
}
