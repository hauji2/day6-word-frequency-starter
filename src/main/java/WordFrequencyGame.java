import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public String getResult(String sentence) {
        String[] words = sentence.split(WordFrequencyConstant.REGEX_SPLITTER);
        if (words.length == 1) {
            return sentence + WordFrequencyConstant.ONE_WITH_PREFIX_SPACE;
        }

        try {
            List<Input> inputList = countWordFrequency(words);
            inputList.sort((previousWord, word) -> word.getWordCount() - previousWord.getWordCount());
            return combineInputsIntoSentenceWithJoiner(inputList);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            return WordFrequencyConstant.CALCULATE_ERROR;
        }
    }

    private String combineInputsIntoSentenceWithJoiner(List<Input> inputList) {
        StringJoiner joiner = new StringJoiner(WordFrequencyConstant.DELIMITER);
        inputList.stream().forEach(input -> joiner.add(input.toWordFrequencySentence()));
        return joiner.toString();
    }

    private List<Input> countWordFrequency(String[] words) {
        List<String> wordList = Arrays.asList(words);

        return wordList.stream()
                .distinct()
                .map(word -> new Input(word, Collections.frequency(wordList, word)))
                .collect(Collectors.toList());
    }
}
